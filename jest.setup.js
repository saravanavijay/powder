import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

window._ = require('lodash');
window.TextEncoder = require('text-encoding').TextEncoder;
window.crypto = require('@trust/webcrypto');
window.moment = require('moment-timezone');

// This is a workaround to a bug in webcrypto with an open PR: https://github.com/anvilresearch/webcrypto/pull/77
window.crypto.__proto__.subtle.__proto__.digest =    // eslint-disable-line no-proto
  (digest => (name, data) => digest({ name }, data))(window.crypto.subtle.digest);

configure({ adapter: new Adapter() });
