import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import PropTypes from 'prop-types';
import React from 'react';
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';

const TestRouter = props => {
  const muiTheme = createMuiTheme({
    ...{
      typography: {
        useNextVariants: true
      }
    },
    ...props.theme
  });
  return (
    <StaticRouter context={{}}>
      <Provider store={props.store}>
        <MuiThemeProvider theme={muiTheme}>
          {props.children}
        </MuiThemeProvider>
      </Provider>
    </StaticRouter>
  );
};

TestRouter.propTypes = {
  children: PropTypes.node.isRequired,
  store: PropTypes.shape({
    dispatch: PropTypes.func.isRequired,
    getState: PropTypes.func.isRequired,
    subscribe: PropTypes.func.isRequired
  }).isRequired,
  theme: PropTypes.shape({})
};

TestRouter.defaultProps = {
  theme: {}
};

export default TestRouter;
